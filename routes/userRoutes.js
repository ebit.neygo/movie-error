const express = require("express");
const router = express.Router();
const userController = require('../controllers/userController');

router.post("/", userController.createUser);
// router.get("/:id", movieValidator.findmovie, movieController.findMovie);
// router.put("/:id", movieValidator.updateMovie, movieController.updateMovie);
// router.delete("/:id", movieValidator.deleteMovie, movieController.deleteMovie);
module.exports = router;