const express = require("express");
const router = express.Router();
const movieController = require("../controllers/movieController");
const movieValidator = require("../middlewares/validators/movieValidator");

router.post("/", movieValidator.createMovie, movieController.createMovie);
router.get("/:id", movieValidator.findmovie, movieController.findMovie);
router.get("/", movieController.getAllMovie);
router.put("/:id", movieValidator.updateMovie, movieController.updateMovie);
router.delete("/:id", movieValidator.deleteMovie, movieController.deleteMovie);
module.exports = router;
