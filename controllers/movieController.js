const crypt = require("crypto");
const path = require("path");
const { movie } = require("../models");

//upload function
function mediaUpload(req, res) {
  if (req.files) {
    const file = req.files.poster;
    let fileName = crypt.randomBytes(16).toString("hex");

    // Rename the file
    file.name = `${fileName}${path.parse(file.name).ext}`;

    // assign req.body.image with file.name
    req.body.poster = file.name;
    file.mv(`./public/images/${file.name}`, async (err) => {
      if (err) {
        console.error(err);
        return res.status(500).json({
          message: "Internal Server Error",
          error: err,
        });
      }
    });
  }
}
class MovieController {
  async createMovie(req, res) {
    try {
      mediaUpload(req, res);
      // req.body.releaseDate = Date.parse(req.body.releaseDate);

      //cast from postman must be parse
      req.body.cast = JSON.parse(req.body.cast);
      req.body.genre = req.body.genre.split(",");
      let data = await movie.create(req.body);
      return res.status(200).json({
        message: "success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error Catch",
        error: e.message,
      });
    }
  }
  async findMovie(req, res) {
    try {
      let movieData = await movie.findOne({ _id: req.params.id });
      if (!movieData) {
        return res.status(404).json({
          message: "Movie Not Found",
        });
      }
      return res.status(200).json({
        message: "Success",
        movieData,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error findMovie",
        error: e,
      });
    }
  }
  async getAllMovie(req, res) {
    try {
      let errors = [];
      let option = {
        page: req.query.page,
        limit: req.query.limit,
        select: "title poster genre",
      };
      let movieData = await movie.paginate({}, option);
      //cari lagi ebit
      console.log(movieData.docs);
      if (movieData.docs.length) {
        res.status(200).json({
          message: "Success",
          movieData,
        });
      } else {
        res.status(404).json({
          message: "page not found!",
        });
      }
      //validate pagination
      // if(){}

      //end of validate
      //end of cari
      // if (!movieData) {
      //   return res.status(404).json({
      //     message: "Movie Not Found",
      //   });
      // }
      // return res.status(200).json({
      //   message: "Success",
      //   movieData,
      // });
    } catch (e) {
      console.log(e);
      return res.status(500).json({
        message: "Internal Server Error find",
        error: e,
      });
    }
  }
  async updateMovie(req, res) {
    try {
      mediaUpload(req, res);
      // req.body.cast = JSON.parse(req.body.cast);
      // console.log(req.body.cast);
      //experiment
      // console.log(Object.keys(movie.schema.obj));
      // genreGen();
      // console.log(req.body.genre[0]);
      //end of exp
      if (req.body.cast != "") {
        req.body.cast = JSON.parse(req.body.cast);
      }
      // console.log(req.body.cast);
      //
      req.body.genre = req.body.genre.split(",");
      // console.log(genres);
      //
      let movieData = await movie.findOneAndUpdate(
        { _id: req.params.id },
        req.body,
        { new: true }
      );
      return res.status(200).json({
        message: "success",
        movieData,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async deleteMovie(req, res) {
    try {
      let deleted = await movie.deleteOne({ _id: req.params.id });
      if (deleted.deletedCount === 0) {
        return res.status(404).json({
          message: "Data Not Found",
        });
      }
      return res.status(200).json({
        message: "success",
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}
module.exports = new MovieController();
