const crypt = require("crypto");
const path = require("path");
const { user } = require("../models");

function mediaUpload(req, res) {
  if (req.files) {
    const file = req.files.poster;
    let fileName = crypt.randomBytes(16).toString("hex");

    // Rename the file
    file.name = `${fileName}${path.parse(file.name).ext}`;

    // assign req.body.image with file.name
    req.body.poster = file.name;
    file.mv(`./public/images/${file.name}`, async (err) => {
      if (err) {
        console.error(err);
        return res.status(500).json({
          message: "Internal Server Error",
          error: err,
        });
      }
    });
  }
}
class UserController {
  async createUser(req, res) {
    try {
      let data = await user.create(req.body);
      return res.status(200).json({
        message: "success",
        data,
      });
    } catch (e) {
      return res.status().json({
        message: "Internal Server Error",
        error: e.message,
      });
    }
  }
}

module.exports = new UserController();
