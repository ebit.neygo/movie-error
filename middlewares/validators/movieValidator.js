const mongoose = require("mongoose");
const validator = require("validator");
const { movie } = require("../../models");
const crypt = require("crypto");
const path = require("path");

exports.createMovie = async (req, res, next) => {
  try {
    let errors = [];
    if (req.files == null) {
      errors.push("select image for Poster");
    }
    if (req.files) {
      const file = req.files.poster;
      if (!file.mimetype.startsWith("image")) {
        errors.push("File must be image!");
      }
    }
    //date validation
    const minDate = new Date(1970);
    const released = new Date(req.body.releaseDate);
    if (released < minDate) {
      errors.push("No Movie before 1970");
    }
    if (errors.length > 0) {
      //Bad request
      return res.status(400).json({
        message: errors.join(", "),
      });
    }
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};
exports.findmovie = async (req, res, next) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return res.status(400).json({
        message:
          "Parameter is not valid and must be 24 character & hexadecimal",
      });
    }

    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};
exports.deleteMovie = async (req, res, next) => {
  try {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return res.status(400).json({
        message:
          "Parameter is not valid and must be 24 character & hexadecimal",
      });
    }
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      erorr: e.message,
    });
  }
};
exports.updateMovie = async (req, res, next) => {
  try {
    //Handle Error for parameter
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return res.status(400).json({
        message:
          "Parameter is not valid and must be 24 character & hexadecimal",
      });
    }

    //Handle error for request
    let errors = [];
    if (req.files == null) {
      errors.push("select image for Poster");
    }
    if (req.files) {
      const file = req.files.poster;
      if (!file.mimetype.startsWith("image")) {
        errors.push("File must be image!");
      }
    }
    //genre validation// just word, comma and space
    let numIgnore = /[^a-zA-Z ,]+/g;
    let result = numIgnore.test(req.body.genre);
    if (result) {
      errors.push("Allowed characters for genre just A-Z,a-z, comma and space");
    }
    if (errors.length > 0) {
      //Bad request
      return res.status(404).json({
        message: errors.join(", "),
      });
    }
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};
