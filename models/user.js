const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const UserSchema = mongoose.Schema({
  userName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  imageUrl: {
    type: String,
    required: false,
  },
  role: {
    type: String,
    required: false,
  },
});

// Enable soft delete
UserSchema.plugin(mongooseDelete, { overrideMethods: "all" });
module.exports = mongoose.model("user", UserSchema, "user");
