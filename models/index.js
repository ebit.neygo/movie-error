const mongoose = require("mongoose");
const uri = process.env.MONGO_URI;
mongoose
  .connect(uri, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log("MongoDB Connected");
  })
  .catch((err) => {
    console.error(err);
  });

const movie = require("./movie");
const user = require("./user");
module.exports = { movie, user };
