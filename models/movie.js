const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");
const mongoosePaginate = require("mongoose-paginate-v2");

const MovieSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    synopsis: {
      type: String,
      required: true,
    },
    releaseDate: {
      type: Date,
      required: true,
    },
    poster: {
      type: String,
      default: null,
      required: true,
      get: getImage, //link
    },
    trailer: {
      //Youtube
      type: String,
      default: null,
      required: true,
    },
    director: {
      type: String,
      required: true,
    },
    duration: {
      type: String,
      required: true,
    },
    budget: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: false,
    },
    genre: [
      {
        type: String,
        required: true,
      },
    ],
    cast: [
      {
        castName: {
          type: String,
          required: true,
        },
        castRole: {
          type: String,
          required: true,
        },
      },
    ],
    song: {
      type: String,
      default: null,
    },
    //masih di awang-awang
    // averageRating: {
    //   type: String,
    //   required: false,
    // },
    // totalReview: {
    //   type: String,
    //   required: false,
    // },
    // ratings: [
    //   {
    //     rating1: {
    //       type: String,
    //       required: false,
    //     },
    //     rating2: {
    //       type: String,
    //       required: false,
    //     },
    //     rating3: {
    //       type: String,
    //       required: false,
    //     },
    //     rating4: {
    //       type: String,
    //       required: false,
    //     },
    //     rating5: {
    //       type: String,
    //       required: false,
    //     },
    //   },
    // ],
    //end of masih di awang-awang
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
    toJSON: { getters: true },
  }
);
function getImage(poster) {
  if (!poster) {
    return null;
  }
  return `/images/${poster}`;
}
// Enable soft delete
MovieSchema.plugin(mongooseDelete, { overrideMethods: "all" });
MovieSchema.plugin(mongoosePaginate);
module.exports = mongoose.model("movie", MovieSchema, "movie");
