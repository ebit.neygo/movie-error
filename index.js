require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});
const express = require("express");
const fileUpload = require("express-fileupload");

const movieRoutes = require("./routes/movieRoutes");
const userRoutes = require('./routes/userRoutes');

//
const app = express();

//
app.use(express.json()); // Enable req.body JSON type
app.use(
  express.urlencoded({
    extended: true,
  })
); // Support urlencode body

// To read form-data request
app.use(fileUpload());

// Set static file directory
app.use(express.static("public"));
app.use("/movie", movieRoutes);
app.use("/user",userRoutes);

app.listen(5000, () => console.log("Server running on 5000"));

module.exports = app;
